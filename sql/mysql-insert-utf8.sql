-- 
-- Base de données :  `festival`
-- MySQL
-- version ETUDIANTS - 2019/2020
--

-- les dirigeants sont fictifs
insert into Etablissement values ('0350785N', 'Collège de Moka', '2 avenue Aristide Briand BP 6', '35401', 'Saint-Malo', '0299206990', null,1,'Monsieur','Dupont','Alain');
insert into Etablissement values ('0350773A', 'Collège Ste Jeanne d''Arc-Choisy', '3, avenue de la Borderie BP 32', '35404', 'Paramé', '0299560159', null, 1,'Madame','Lefort','Anne');  
insert into Etablissement values ('0352072M', 'Institution Saint-Malo Providence', '2 rue du collège BP 31863', '35418', 'Saint-Malo', '0299407474', null, 1,'Monsieur','Durand','Pierre');   
insert into Etablissement values ('0352025K', 'Centre de rencontres internationales', '37 avenue du R.P. Umbricht BP 108', '35407', 'Saint-Malo', '0299000000', null, 0, 'Monsieur','Guenroc','Guy');

insert into TypeChambre values ('C1', '1 lit');
insert into TypeChambre values ('C2', '2 à 3 lits');
insert into TypeChambre values ('C3', '4 à 5 lits');
insert into TypeChambre values ('C4', '6 à 8 lits');
insert into TypeChambre values ('C5', '8 à 12 lits');
 
-- certains groupes sont incomplètement renseignés
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g001','Groupe folklorique du Bachkortostan',40,'Bachkirie','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g002','Marina Prudencio Chavez',25,'Bolivie','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g003','Nangola Bahia de Salvador',34,'Brésil','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g004','Bizone de Kawarma',38,'Bulgarie','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g005','Groupe folklorique camerounais',22,'Cameroun','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g006','Syoung Yaru Mask Dance Group',29,'Corée du Sud','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g007','Pipe Band',19,'Ecosse','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g008','Aira da Pedra',5,'Espagne','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g009','The Jersey Caledonian Pipe Band',21,'Jersey','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g010','Groupe folklorique des Émirats',30,'Emirats arabes unis','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g011','Groupe folklorique mexicain',38,'Mexique','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g012','Groupe folklorique de Panama',22,'Panama','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g013','Groupe folklorique papou',13,'Papouasie','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g014','Paraguay Ete',26,'Paraguay','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g015','La Tuque Bleue',8,'Québec','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g016','Ensemble Leissen de Oufa',40,'République de Bachkirie','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g017','Groupe folklorique turc',40,'Turquie','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g018','Groupe folklorique russe',43,'Russie','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g019','Ruhunu Ballet du village de Kosgoda',27,'Sri Lanka','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g020','L''Alen',34,'France - Provence','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g021','L''escolo Di Tourre',40,'France - Provence','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g022','Deloubes Kévin',1,'France - Bretagne','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g023','Daonie See',5,'France - Bretagne','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g024','Boxty',5,'France - Bretagne','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g025','Soeurs Chauvel',2,'France - Bretagne','O');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g026','Cercle Gwik Alet',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g027','Bagad Quic En Groigne',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g028','Penn Treuz',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g029','Savidan Launay',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g030','Cercle Boked Er Lann',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g031','Bagad Montfortais',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g032','Vent de Noroise',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g033','Cercle Strollad',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g034','Bagad An Hanternoz',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g035','Cercle Ar Vro Melenig',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g036','Cercle An Abadenn Nevez',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g037','Kerc''h Keltiek Roazhon',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g038','Bagad Plougastel',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g039','Bagad Nozeganed Bro Porh-Loeiz',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g040','Bagad Nozeganed Bro Porh-Loeiz',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g041','Jackie Molard Quartet',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g042','Deomp',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g043','Cercle Olivier de Clisson',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g044','Kan Tri',0,'France - Bretagne','N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g045', 'Panama Fuerte Raza', 14, 'Panama', 'N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g046', 'Le Ballet Rey', 25, 'Bielorussie', 'N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g047', 'Soïg Siberil et Etienne Grandjean', 2, 'France - Bretagne', 'N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g048', 'Le Bour-Bodros', 3, 'France-Bretagne', 'N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g049', 'Ensemble Kidra Budaya', 15, 'Indonésie', 'N');
insert into Groupe (id, nom, nombrepersonnes, nompays, hebergement) values ('g050', ' Compagna Folklorica  Camagua', 20, 'Cuba', 'N');

-- les offres sont fictives
insert into Offre values ('0350785N', 'C1', 5);
insert into Offre values ('0350785N', 'C2', 10);
insert into Offre values ('0350785N', 'C3', 5);

insert into Offre values ('0350773A', 'C2', 15);
insert into Offre values ('0350773A', 'C3', 1);

insert into Offre values ('0352072M', 'C1', 5);
insert into Offre values ('0352072M', 'C2', 10);
insert into Offre values ('0352072M', 'C3', 3);

-- les attributions sont fictives
insert into Attribution values ('0350785N', 'C1', 'g001', 1);
insert into Attribution values ('0350785N', 'C1', 'g002', 2);
insert into Attribution values ('0350785N', 'C1', 'g003', 2);
insert into Attribution values ('0350785N', 'C2', 'g001', 2);
insert into Attribution values ('0350785N', 'C2', 'g002', 1);
insert into Attribution values ('0350785N', 'C3', 'g001', 2);
insert into Attribution values ('0350785N', 'C3', 'g002', 1);

insert into Attribution values ('0350773A', 'C2', 'g004', 2);
insert into Attribution values ('0350773A', 'C3', 'g005', 1);

insert into Attribution values ('0352072M', 'C1', 'g006', 1);
insert into Attribution values ('0352072M', 'C2', 'g007', 3);
insert into Attribution values ('0352072M', 'C3', 'g006', 3);

INSERT INTO Utilisateur VALUES (1,'Madame','Aubert', 'Lise', 'laubert@saint-malo.fr', 'laubert', 'mdplaubert');
INSERT INTO Utilisateur VALUES (2,'Monsieur','Dupont', 'Alain', 'adupont@saint-malo.fr', 'adupont', 'mdpadupont');
INSERT INTO Utilisateur VALUES (3,'Madame','Joubert', 'Julie', 'jjoubert@saint-malo.fr', 'jjoubert', 'mdpjjoubert');

UPDATE Utilisateur
SET mdp = MD5(mdp);

INSERT INTO Lieu (id, nom, adresse, capaciteAccueil) VALUES ("1", "SALLE DU PANIER FLEURI", "Rue de Bonneville", "450");
INSERT INTO Lieu (id, nom, adresse, capaciteAccueil) VALUES ("2", "LE CABARET", "MAIRIE ANNEXE DE PARAME,Place Georges COUDRAY", "250");
INSERT INTO Lieu (id, nom, adresse, capaciteAccueil) VALUES ("3", "LE PARC DES CHENES", "14 rue des chênes", "2000");
INSERT INTO Lieu (id, nom, adresse, capaciteAccueil) VALUES ("4", "LE VILLAGE", "Ecole LEGATELOIS,25 rue Général de Castelnau", "500");

INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("1", "1", "G045", "2019-10-15", "17:20:00", "19:20:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("2", "2","G012",  "2019-10-16", "18:20:00", "20:00:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("3", "4","G016" , "2019-10-17", "17:50:00", "19:00:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("4", "4", "G050", "2019-10-18", "19:20:00", "22:00:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("5", "1", "G015", "2019-10-19", "17:20:00", "19:20:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("6", "4", "G018", "2019-10-20", "22:40:00", "23:50:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("7", "2", "G041", "2019-10-20", "15:30:00", "18:00:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("8", "2", "G023", "2019-10-15", "18:30:00", "19:50:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("9", "3", "G029", "2019-10-16", "16:35:00", "19:45:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("10", "4", "G027", "2019-10-16", "17:40:00", "19:00:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("11", "1", "G028", "2019-10-16", "19:45:00", "23:00:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("12", "2", "G047", "2019-10-18", "22:00:00", "23:50:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("13", "4", "G026", "2019-10-17", "14:30:00", "17:00:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("14", "3", "G035", "2019-10-16", "20:20:00", "23:50:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("15", "1", "G034", "2019-10-18", "20:00:00", "22:30:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("16", "1", "G030", "2019-10-15", "19:40:00", "22:45:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("17", "2", "G020", "2019-10-19", "20:20:00", "23:20:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("18", "3", "G018", "2019-10-20", "16:20:00", "18:40:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("19", "1", "G002", "2019-10-20", "19:30:00", "22:30:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("20", "3", "G009", "2019-10-17", "19:20:00", "21:50:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("21", "3", "G017", "2019-10-18", "22:00:00", "23:30:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("22", "3", "G042", "2019-10-17", "16:20:00", "19:00:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("23", "1", "G032", "2019-10-20", "15:20:00", "17:30:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("24", "1", "G039", "2019-10-17", "16:30:00", "19:30:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("25", "4", "G047", "2019-10-20", "17:50:00", "20:10:00");
INSERT INTO Representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("26", "2", "G005", "2019-10-15", "20:10:00", "23:20:00");
