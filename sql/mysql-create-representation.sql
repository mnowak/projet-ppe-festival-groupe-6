USE festival;

DROP TABLE representation;

CREATE TABLE representation (
id int(11) PRIMARY KEY,
id_groupe VARCHAR(4) NOT NULL,
id_lieu VARCHAR(10) NOT NULL,
date_Rep DATE NOT NULL,
heure_debut TIME NOT NULL,
heure_fin TIME NOT NULL,
CONSTRAINT fk_presentation_groupe FOREIGN KEY (id_groupe) REFERENCES groupe (id),
CONSTRAINT fk_presentation_lieu FOREIGN KEY (id_lieu) REFERENCES lieu (id)
);

