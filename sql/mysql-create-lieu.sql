USE festival;

DROP TABLE Lieu;

create table lieu (
    id varchar(250) not null, 
    nom TEXT not null,
    adresse TEXT not null,
    capaciteAccueil int(10) not null,
    constraint pk_Lieu primary key(id)

)ENGINE=INNODB;




