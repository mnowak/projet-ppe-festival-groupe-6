USE festival;

INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("1", "1", "G045", "2019-10-15", "17:20:00", "19:20:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("2", "2","G012",  "2019-10-16", "18:20:00", "20:00:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("3", "4","G016" , "2019-10-17", "17:50:00", "19:00:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("4", "4", "G050", "2019-10-18", "19:20:00", "22:00:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("5", "1", "G015", "2019-10-19", "17:20:00", "19:20:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("6", "4", "G018", "2019-10-20", "22:40:00", "23:50:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("7", "2", "G041", "2019-10-20", "15:30:00", "18:00:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("8", "2", "G023", "2019-10-15", "18:30:00", "19:50:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("9", "3", "G029", "2019-10-16", "16:35:00", "19:45:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("10", "4", "G027", "2019-10-16", "17:40:00", "19:00:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("11", "1", "G028", "2019-10-16", "19:45:00", "23:00:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("12", "2", "G047", "2019-10-18", "22:00:00", "23:50:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("13", "4", "G026", "2019-10-17", "14:30:00", "17:00:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("14", "3", "G035", "2019-10-16", "20:20:00", "23:50:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("15", "1", "G034", "2019-10-18", "20:00:00", "22:30:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("16", "1", "G030", "2019-10-15", "19:40:00", "22:45:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("17", "2", "G020", "2019-10-19", "20:20:00", "23:20:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("18", "3", "G018", "2019-10-20", "16:20:00", "18:40:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("19", "1", "G002", "2019-10-20", "19:30:00", "22:30:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("20", "3", "G009", "2019-10-17", "19:20:00", "21:50:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("21", "3", "G017", "2019-10-18", "22:00:00", "23:30:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("22", "3", "G042", "2019-10-17", "16:20:00", "19:00:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("23", "1", "G032", "2019-10-20", "15:20:00", "17:30:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("24", "1", "G039", "2019-10-17", "16:30:00", "19:30:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("25", "4", "G047", "2019-10-20", "17:50:00", "20:10:00");
INSERT INTO representation (id, id_lieu, id_groupe, date_Rep, heure_debut, heure_fin) VALUES ("26", "2", "G005", "2019-10-15", "20:10:00", "23:20:00");

