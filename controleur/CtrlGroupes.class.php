<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controleur;


use controleur\GestionErreurs;
use modele\dao\GroupeDAO;
use modele\dao\AttributionDAO;
use modele\metier\Groupe;
use modele\dao\Bdd;
use vue\groupes\VueListeGroupes;
use vue\groupes\VueDetailGroupes;
use vue\groupes\VueSaisieGroupes;
use vue\groupes\VueSupprimerGroupes;
/**
 * Description of CtrlGroupes
 *
 * @author maxence
 */
class CtrlGroupes extends ControleurGenerique  {
   /** controleur= etablissements & action= defaut
     * Afficher la liste des établissements      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= groupes & action= liste
     * Afficher la liste des établissements      */
    public function liste() {
        $laVue = new VueListeGroupes();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des groupes 
        Bdd::connecter();
        $listeGroupes = GroupeDao::getAll();
        $laVue->setLesGroupes($listeGroupes);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=detail & id=identifiant_groupe
     * Afficher un groupe d'après son identifiant     */
    public function detail() {
        $idGroupe = $_GET["id"];
        $this->vue = new VueDetailGroupes();
        // Lire dans la BDD les données du groupe à afficher
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGroupe));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=creer
     * Afficher le formulaire d'ajout d'un groupe     */
    public function creer() {
        $laVue = new VueSaisieGroupes();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvel groupe");
        // En création, on affiche un formulaire vide
        /* @var Groupe $unGroupe */
        $unGroupe = new Groupe("", "", "", "", "", "", "");
        $laVue->setUnGroupe($unGroupe);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=validerCreer
     * ajouter d'un groupe dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un groupe */
        $unGroupe = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['adresseRue'], $_REQUEST['Responsable'], $_REQUEST['NombrePersonne'], $_REQUEST["PaysOrigine"], $_REQUEST["hebergement"]);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesGroupe($unGroupe, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer le groupe
            GroupeDAO::insert($unGroupe);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieGroupes();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvel établissement");
            $laVue->setUnGroupe($unGroupe);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupes");
            $this->vue->afficher();
        }
    }

    /** controleur= groupes & action=modifier $ id=identifiant du groupe à modifier
     * Afficher le formulaire de modification d'un groupe     */
    public function modifier() {
        $idGroupe = $_GET["id"];
        $laVue = new VueSaisieGroupes();
        $this->vue = $laVue;
        // Lire dans la BDD les données du groupe à modifier
        Bdd::connecter();
        /* @var Groupe $leGroupe */
        $leGroupe = GroupeDAO::getOneById($idGroupe);
        $this->vue->setUnGroupe($leGroupe);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier le groupe : " . $leGroupe->getNom());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=validerModifier
     * modifier un groupe dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un groupe */
        $unGroupe = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['adresseRue'], $_REQUEST['Responsable'], $_REQUEST['NombrePersonne'], $_REQUEST["PaysOrigine"], $_REQUEST["hebergement"]);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesGroupe($unGroupe, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour le groupe
            GroupeDAO::update($unGroupe->getId(), $unGroupe);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieGroupes();
            $this->vue = $laVue;
            $laVue->setUnGroupe($unGroupe);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier le groupe : " . $unGroupe->getNom());
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupes");
            $this->vue->afficher();
        }
    }

    /** controleur= groupes & action=supprimer & id=identifiant_groupe
     * Supprimer un groupe d'après son identifiant     */
    public function supprimer() {
        $idGroupe = $_GET["id"];
        $this->vue = new VueSupprimerGroupes();
        // Lire dans la BDD les données du groupe  à supprimer
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGroupe));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action= validerSupprimer
     * supprimer un groupe dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant du groupe à supprimer");
        } else {
            // suppression du groupe d'après son identifiant
            GroupeDAO::delete($_GET["id"]);
        }
        // retour à la liste des groupes
        header("Location: index.php?controleur=groupes&action=liste");
    }

    /**
     * Vérification des données du formulaire de saisie
     * @param Groupe $unGroupe groupe à vérifier
     * @param bool $creation : =true si formulaire de création d'un nouveau groupe ; =false sinon
     */
    private function verifierDonneesGroupe(Groupe $unGroupe, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $unGroupe->getId() == "") || $unGroupe->getNom() == "" || $unGroupe->getHebergement() == "" || $unGroupe->getNomPays() == "" ||
                $unGroupe->getNbPers() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $unGroupe->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($unGroupe->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (GroupeDAO::isAnExistingName($creation, $unGroupe->getId(), $unGroupe->getNom())) {
                    GestionErreurs::ajouter("Le groupe " . $unGroupe->getNom() . " existe déjà");
                }
            }
        }
        // Vérification qu'un groupe de même nom n'existe pas déjà (id + nom si création)
        if ($unGroupe->getNom() != "" && GroupeDAO::isAnExistingName($creation, $unGroupe->getId(), $unGroupe->getNom())) {
            GestionErreurs::ajouter("Le groupe " . $unGroupe->getNom() . " existe déjà");
        }
        
       if ($unGroupe->getNbPers() <= 0 ) {
            GestionErreurs::ajouter("Le groupe " . $unGroupe->getNom() . " doit avoir un nombre de membre superieur à 0");
        }  
    }
    public function getLesGroupes(): Array {
        $lesGroupes = GroupeDAO::getAll();
        return $lesGroupes;}  
}
