<?php

/**
 * Contrôleur de gestion des representations
 * @author maxence
 * @version 2019
 */

namespace controleur;

use modele\dao\EtablissementDAO;
use modele\dao\TypeChambreDAO;
use modele\dao\RepresentationDAO;
use modele\dao\GroupeDAO;
use modele\dao\LieuDAO;
use modele\dao\Bdd;
use modele\metier\Representation;
use modele\metier\Lieu;
use modele\metier\Groupe;
use vue\representations\VueConsultationRepresentations;
use vue\representations\VueSupprimerRepresentation;
use vue\representations\VueSaisieRepresentations;

class CtrlRepresentation extends ControleurGenerique {

    /** controleur= representation & action= defaut
     * Afficher la liste des representation      */
    public function defaut() {
        $this->consulter();
    }

    /** controleur= representation & action= consulter
     * Afficher la liste des representations      */
    function consulter() {
        $laVue = new VueConsultationRepresentations();
        $this->vue = $laVue;
        // La vue a besoin de la liste des groupes et des lieux
        Bdd::connecter();
        $laVue->setLesRepresentations(RepresentationDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action= modifier & id = identifiant du groupe visé
     * Modifier les representations  */
    function modifier() {
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        // Lecture de l'id de la representation
        $idRep = $_GET['id'];

        Bdd::connecter();
        $uneRepresentation = RepresentationDAO::getOneById($idRep);
        $laVue->setUneRepresentation($uneRepresentation);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setGroupes(GroupeDAO::getAll());
        $laVue->setLieux(LieuDAO::getAll());
        $laVue->setMessage("Modifier la representation du groupe : " . $uneRepresentation->getGroupe()->getNom() . " du " . $uneRepresentation->getDateRep() . " à " . $uneRepresentation->getLieu()->getNom());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action=validerModifier
     * modifier une representation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un groupe */
        $groupe = GroupeDAO::getOneById($_REQUEST['groupes']);
        $lieu = LieuDAO::getOneById($_REQUEST['lieux']);
        $uneRepresentation = new Representation($_REQUEST['id'], $groupe, $lieu, $_REQUEST['dateRep'], $_REQUEST['heureD'], $_REQUEST['heureF']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesRepresentation($uneRepresentation, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour la representation
            RepresentationDAO::update($uneRepresentation->getId(), $uneRepresentation);
            // revenir à la liste des representations
            header("Location: index.php?controleur=representation&action=consulter");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRepresentation);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setGroupes(GroupeDAO::getAll());
            $laVue->setLieux(LieuDAO::getAll());
            $laVue->setMessage("Modifier la representation du groupe : " . $uneRepresentation->getGroupe()->getNom() . " du " . $uneRepresentation->getDateRep() . " à " . $uneRepresentation->getLieu()->getNom());
            parent::controlerVueAutorisee();
            $this->vue->setTitre("Festival - representation");
            $this->vue->afficher();
        }
    }

//     private function verifierDonneesRepresentation(Representation $uneRepresentation, bool $creation) {
//        // Vérification des champs obligatoires.
//        // Dans le cas d'une création, on vérifie aussi les champs
//        if (($creation && $uneRepresentation->getGroupe() == "") || $uneRepresentation->getLieu() == "" || $uneRepresentation->getDateRep() == "" || $uneRepresentation->getHeureD() == "" ||
//                $uneRepresentation->getHeureF() == "") {
//            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
//        }
//        if ($uneRepresentation->getHeureD() >= $uneRepresentation->getHeureF()) {
//            GestionErreurs::ajouter("Probleme dans les heures saisies. Le debut de la representation ne peut pas être après sa fin !");
//        }
//        // En cas de création, vérification de la non existence
//        if (($creation && $uneRepresentation->getGroupe() == "") || $uneRepresentation->getLieu() == "" || $uneRepresentation->getDateRep() == "" || $uneRepresentation->getHeureD() == "" ||
//                $uneRepresentation->getHeureF() == "") {
//            
//                if (RepresentationDAO::isAnExistingId($uneRepresentation->getId()) && RepresentationDAO::isAnExistingRep($creation, $uneRepresentation) ) {
//                    GestionErreurs::ajouter("La representation du groupe  " . $uneRepresentation->getGroupe()->getNom() ." à " .$uneRepresentation->getLei()->getNom() . " le " .$uneRepresentation->getDateRep() . " existe déjà");
//                }
//        }
//    }
    private function verifierDonneesRepresentation(Representation $uneRep, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $uneRep->getId() == "") || $uneRep->getGroupe() == "" || $uneRep->getLieu() == "" || $uneRep->getHeureD() == "" ||
            $uneRep->getHeureF() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }

        if ($creation) {
            if (RepresentationDAO::isAnExistingId($uneRep->getId())) {
                GestionErreurs::ajouter("La représentation " . $uneRep->getId() . " existe déjà");
            }
        }

        // Vérification qu'une représentation ne soit pas sur le même lieu en même temps qu'une autre représentation
        if ($uneRep->getLieu() != "" && $uneRep->getHeureD() != "" && $uneRep->getHeureF() != "" && RepresentationDAO::isAnExistingRep($creation, $uneRep)) {
            GestionErreurs::ajouter("Une représentation ce passe déjà à " . $uneRep->getLieu()->getNom() . " entre " . $uneRep->getHeureD() . " et " . $uneRep->getHeureF() . ".");
        }
        // Vérification que le groupe soit déjà dans une représentation
        if ($uneRep->getGroupe() != "" && RepresentationDAO::isAnExistingGrp($creation, $uneRep->getGroupe())) {
            GestionErreurs::ajouter("Le groupe " . $uneRep->getGroupe()->getNom() . " a déjà une représentation.");
        }

        // Vérification que l'heure de début ne soit pas supérieur à l'heure de fin
        if( $uneRep->getHeureD() >=  $uneRep->getHeureF()){
             GestionErreurs::ajouter("L'heure de début ne doit pas être supérieur à l'heure de fin.");
        }
    }

    /** controleur= representation & action=supprimer & id=identifiant_groupe
     * Supprimer une representation d'après son identifiant     */
    public function supprimer() {
        $idRep = $_GET["id"];
        $this->vue = new VueSupprimerRepresentation();
        // Lire dans la BDD les données de la representation à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRep));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action= validerSupprimer
     * supprimer une representation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la representation a supprimer");
        } else {
            // suppression de la representation d'après son identifiant
            RepresentationDAO::delete($_GET["id"]);
        }
        // retour à la liste des groupes
        header("Location: index.php?controleur=representation&action=consulter");
    }

    /** controleur= representation & action=creer
     * Afficher le formulaire d'ajout d'une representation     */
    public function creer() {
        Bdd::connecter();
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvelle representation");
        $laVue->setLieux(LieuDAO::getAll());
        $laVue->setGroupes(GroupeDAO::getAll());
        // En création, on affiche un formulaire vide
        /* @var Representation $uneRepresentation */
        $lieu = new Lieu(0, "", "", 0);
        $groupe = new Groupe("", "", "", "", 0, "", "");
        $uneRep = new Representation(0, $groupe, $lieu, "", "00:00:00", "00:00:00");

        $laVue->setUneRepresentation($uneRep);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representations");
        $this->vue->afficher();
    }

    /** controleur= representation & action=validerCreer
     * ajouter d'une representation dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        $groupe = GroupeDAO::getOneById($_REQUEST['groupes']);
        $lieu = LieuDAO::getOneById((int)$_REQUEST['lieux']);        /* @var Representation $uneRepresentation  : récupération du contenu du formulaire et instanciation d'une representation */
        $uneRepresentation = new Representation($_REQUEST['id'], $groupe, $lieu, $_REQUEST['dateRep'], $_REQUEST['heureD'], $_REQUEST["heureF"]);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesRepresentation($uneRepresentation, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer la representation
            RepresentationDAO::insert($uneRepresentation);
            // revenir à la liste des representations
            header("Location: index.php?controleur=representation&action=consulter");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvelle representation");
            $laVue->setUneRepresentation($uneRepresentation);
            $laVue->setLieux(LieuDAO::getAll());
            $laVue->setGroupes(GroupeDAO::getAll());
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - representations");
            $this->vue->afficher();
        }
    }

}
