<?php

namespace modele\metier;
class Lieu {
      private $id;
    /**
     * nom du lieu
     * @var string
     */
    private $nom;
    /**
     * $capacite d'Accueil du lieu
     * @var string 
     */
    private $capaciteAccueil;
    /**
     * adresse du lieu
     * @var string
     */
    private $adresse;

    function __construct($id, $nom, $adresse, $capaciteAccueil) {
        $this->id = $id;
        $this->nom = $nom;
        $this->capaciteAccueil = $capaciteAccueil;
        $this->adresse = $adresse;
    }
    function getId() {
        return $this->id;
    }
    function getNom() {
        return $this->nom;
    }
    function getCapaciteAccueil() {
        return $this->capaciteAccueil;
    }
    function getAdresse() {
        return $this->adresse;
    }
    function setId($id) {
        $this->id = $id;
    }
    function setNom($nom) {
        $this->nom = $nom;
    }
    function setCapaciteAccueil($capaciteAccueil) {
        $this->capaciteAccueil = $capaciteAccueil;
    }
    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }
}
