<?php


namespace modele\metier;

class Representation {
    
    /**
     * id du groupe
     * @var String
     */
    private $id;
    /**
     * id du groupe
     * @var Groupe
     */
    private $groupe;
    /**
     * id du lieu
     * @var  Lieu
     */
    private $lieu;
    /**
     * date de repetition
     * @var Date
     */
    private $dateRep;
    /**
     * Heure de debut
     * @var string
     */
    private $heureD;
    /**
     * Heure de fin
     * @var string
     */
    private $heureF;
    
    

    function __construct($id,$groupe,$lieu,$dateRep, $heureD,$heureF) {
        $this->id = $id;
        $this->groupe = $groupe;
        $this->lieu = $lieu;
        $this->dateRep = $dateRep;
        $this->heureD = $heureD;
        $this->heureF = $heureF;
    }
    
    function getId() {
        return $this->id;
    }

    function getGroupe() {
        return $this->groupe;
    }

    function getLieu() {
        return $this->lieu;
    }

    function getDateRep() {
        return $this->dateRep;
    }

    function getHeureD() {
        return $this->heureD;
    }

    function getHeureF() {
        return $this->heureF;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setGroupe($groupe) {
        $this->groupe = $groupe;
    }

    function setLieu($lieu) {
        $this->lieu = $lieu;
    }

    function setDateRep($dateRep) {
        $this->dateRep = $dateRep;
    }

    function setHeureD($heureD) {
        $this->heureD = $heureD;
    }

    function setHeureF($heureF) {
        $this->heureF = $heureF;
    }

}
