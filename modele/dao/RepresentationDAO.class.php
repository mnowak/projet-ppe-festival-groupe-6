<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele\dao;

use modele\metier\Representation;
use modele\metier\Lieu;
use modele\metier\Groupe;
use modele\dao\LieuDAO;
use modele\dao\GroupeDAO;
use PDO;
use PDOStatement;

class RepresentationDAO {
     /**
     * Instancier un objet de la classe Representation à partir d'un enregistrement de la table REPRESENTATION
     * @param array $enreg
     * @return Representation
     */
     protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $Groupe = GroupeDAO::getOneById($enreg['ID_GROUPE']);
        $Lieu = LieuDAO::getOneById($enreg['ID_LIEU']);
        $dateRep = $enreg['DATE_REP'];
        $heureD = $enreg['HEURE_DEBUT'];
        $heureF = $enreg['HEURE_FIN'];
 
        // instancier l'objet Representation
        $objetRepresentation = new Representation($id,$Groupe, $Lieu,$dateRep,$heureD,$heureF);
        
        return $objetRepresentation;
    }

    
    /**
     * Complète une requête préparée
     * les paramètres de la requête associés aux valeurs des attributs d'un objet métier
     * @param Representation $objetMetier
     * @param PDOStatement $stmt
     */
    protected static function metierVersEnreg(Representation $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires

        
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':idGroupe', $objetMetier->getGroupe()->getId());
        $stmt->bindValue(':idLieu', $objetMetier->getLieu()->getId());
        $stmt->bindValue(':dateRep', $objetMetier->getDateRep());
        $stmt->bindValue(':heureD', $objetMetier->getHeureD());
        $stmt->bindValue(':heureF', $objetMetier->getHeureF());
    }
    
    /**
     * Retourne la liste de toutes les representations
     * @return array tableau d'objets de type Representation
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY DATE_REP, ID_LIEU, HEURE_DEBUT";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Construire un objet d'après son identifiant, à partir des enregistrements de la table Representation
     * L'identifiant de la table Representation est composé : ($id,$idGroupe,$idLieu,$dateRep,$heureD,$heureF)
     * @param string $id identifiant de la representation
     * @return Representation : objet métier si trouvé dans la BDD, null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }

        return $objetConstruit;
    }
    
    
    /**
     * Détruire un enregistrement de la table Representation d'après son identifiant
     * @param string $id identifiant de la representation
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    
    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Representation objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $ok = false;
        $requete = "INSERT INTO Representation "
                . "  VALUES(:id, :idGroupe,:idLieu,:dateRep,:heureD, :heureF)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    
    
    /**
     * Mise à jour des informations d'une representation
     * @param string $id identifiant de la representation
     * @return boolean =true si la mise à jour a été correcte
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        
        $requete = "UPDATE Representation "
                . " SET ID_GROUPE=:idGroupe, ID_LIEU=:idLieu, DATE_REP=:dateRep, HEURE_DEBUT=:heureD, HEURE_FIN=:heureF "
                . " WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    
    /**
     * Permet de vérifier s'il existe ou non une representation ayant déjà le même identifiant dans la BD
     * @param string $id identifiant de la representation à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id)
    {
        $requete = "SELECT COUNT(*) FROM Representation WHERE id =:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }

    /**
     * Permet de vérifier si le lieu de la représentation est déjà sur une autre représentation à la même heure
     * @param bool $creation savoir si c'est une nouvelle représentation ou un modification
     * @param Representation $uneRep l'objet représentation
     * @return boolean =true si la lieu est déjà utilisé, =false sinon
     */
    public static function isAnExistingRep($creation, $uneRep)
    {
        // S'il s'agit d'une création, on vérifie juste la non existence du nom sinon
        // on vérifie la non existence d'un autre établissement (id!='$id') portant
        // le même nom
        $id = $uneRep->getId();
        $hrFn = $uneRep->getHeureF();
        $hrDbt = $uneRep->getHeureD();
        $date = $uneRep->getDateRep();
        $idLieu = $uneRep->getLieu()->getId();
        if ($creation) {
            $requete = "SELECT COUNT(*) FROM Representation WHERE ID_LIEU = :idlieu AND DATE_REP = :dateRep AND (heure_Debut > :hrDbt AND heure_Debut < :hrFn AND heure_Fin > :hrDbt AND heure_Fin < :hrFn OR :hrDbt >= heure_Debut AND :hrDbt <= heure_Fin OR :hrFn >= heure_Debut AND :hrFn <= heure_Fin)";
            $stmt = Bdd::getPdo()->prepare($requete);
            $stmt->bindParam(':idlieu', $idLieu);
            $stmt->bindParam(':hrDbt', $hrDbt);
            $stmt->bindParam(':hrFn', $hrFn);
            $stmt->bindParam(':dateRep', $date);
            $stmt->execute();
        } else {
            $requete = "SELECT COUNT(*) FROM Representation WHERE id <> :id AND ID_LIEU = :idlieu AND DATE_REP = :dateRep AND (heure_Debut > :hrDbt AND heure_Debut < :hrFn AND heure_Fin > :hrDbt AND heure_Fin < :hrFn OR :hrDbt >= heure_Debut AND :hrDbt <= heure_Fin OR :hrFn >= heure_Debut AND :hrFn <= heure_Fin)";
            $stmt = Bdd::getPdo()->prepare($requete);
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':idlieu', $idLieu);
            $stmt->bindParam(':hrDbt', $hrDbt);
            $stmt->bindParam(':hrFn', $hrFn);
            $stmt->bindParam(':dateRep', $date);
            $stmt->execute();
        }
        return $stmt->fetchColumn(0);
    }

    /**
     * Permet de vérifier si le groupe n'a pas déjà une représentation
     * @param bool $creation savoir si c'est une nouvelle représentation ou un modification
     * @param Groupe $groupe groupe de la représentation
     * @return boolean =true si la lieu est déjà utilisé, =false sinon
     */
    public static function isAnExistingGrp($creation, $groupe)
    {
        $ok = false;
        $idGrp = $groupe->getId();
        if ($creation) {
            $requete = "SELECT COUNT(*) FROM Representation WHERE ID_GROUPE =:idgroupe";
            $stmt = Bdd::getPdo()->prepare($requete);
            $stmt->bindParam(':idgroupe', $idGrp);
            $stmt->execute();
            if ($stmt->fetchColumn(0)) {
                $ok = true;
            }
        }
        return $ok;
    }
}
