<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Representation Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
        use modele\metier\Groupe;
        use modele\metier\Lieu;
        
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Representation</h2>";
        $groupe = new Groupe(1,"grouep de musique","michel","mouzeil",15,"France","O");
        $lieu = new Lieu(1, "utchebe", "nantes", 56);
        $objet = new Representation(1,$groupe,$lieu,"15-12-2015","17:00:00","19:00:00");
        var_dump($objet);
        
        ?>
    </body>
</html>