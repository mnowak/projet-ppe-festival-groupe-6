<?php
namespace vue\representations;
use vue\VueGenerique;
use modele\metier\Representation;

class VueConsultationRepresentations extends VueGenerique {

    /** @var array liste des Representations */
    private $lesRepresentations;
    
    // @var array Liste des Dates
    private $laDate = 0;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();

        // IL FAUT QU'IL Y AIT AU MOINS UNE REPRESENTATION POUR QUE L'AFFICHAGE SOIT EFFECTUÉ        
        if (count($this->lesRepresentations) != 0) {
            // POUR CHAQUE REPRESENTATION : AFFICHAGE DE LA DATE ET D'UN TABLEAU COMPORTANT 1
            // LIGNE D'EN-TÊTE ET 1 LIGNE PAR REPRESENTATION
            foreach ($this->lesRepresentations as $uneRepresentation) {
                //Si la Représentation possède une nouvelle date, on créé un nouveau tableau
                if($this->laDate != $uneRepresentation->getDateRep()){
                    //Si il n'y a plus de représentations (et donc plus de date), arretez de créer des tableaux
                    if ($this->laDate != 0) { ?> </table> <br/> <?php } ?>
                        <strong><?= $uneRepresentation->getDateRep() ?></strong><br/>
                        <table width="45%" cellspacing="0" cellpadding="0" class="tabQuadrille">
                        <!--AFFICHAGE DE LA LIGNE D'EN-TÊTE-->
                        <tr class="enTeteTabQuad">
                            <td width="33%">Lieu</td>
                            <td width="35%">Groupe</td>
                            <td width="8%">Heure Début</td> 
                            <td width="8%">Heure Fin</td>
                            <td width="8%">Modifier</td>
                            <td width="8%">Supprimer</td>
                            
                        </tr>
                <?php
                }
                ?>
                <tr class="ligneTabQuad">
                    <td><?= $uneRepresentation->getLieu()->getNom() ?></td>
                    <td><?= $uneRepresentation->getGroupe()->getNom() ?></td>
                    <td><?= $uneRepresentation->getHeureD() ?></td>
                    <td><?= $uneRepresentation->getHeureF() ?></td>
                    <td><a href="index.php?controleur=representation&action=modifier&id=<?= $uneRepresentation->getId() ?>" > Modifier </a></td>
                    <td><a href="index.php?controleur=representation&action=supprimer&id=<?= $uneRepresentation->getId() ?>" > Supprimer </a></td>

                </tr>
                <?php
                $this->laDate = $uneRepresentation->getDateRep();
            }
            
            include $this->getPied();
        }        
        else {
        ?>
        <strong>Il n'y a pas de représentations pour les jours prochains</strong>
    <?php
        }
        ?>
                <a href="index.php?controleur=representation&action=creer&id=<?= $uneRepresentation->getId() ?>" > Ajouter </a>
            <?php
    }

    public function setLesDates(array $lesDates) {
        $this->lesDates = $lesDates;
    }

    public function setLesRepresentations(array $lesRepresentations) {
        $this->lesRepresentations = $lesRepresentations;
    }
    

}
