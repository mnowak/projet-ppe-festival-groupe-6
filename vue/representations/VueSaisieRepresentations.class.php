<?php
namespace vue\representations;
use vue\VueGenerique;
use modele\metier\Representation;
use modele\metier\Groupe;
use modele\metier\Lieu;

/**
 * Description of VueSaisieRepresentation
 
 * @author maxen
 */
class VueSaisieRepresentations extends VueGenerique {

    /** @var Representation representation à afficher */
    private $uneRepresentation;

    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;

    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;

    /** @var string à afficher en tête du tableau */
    private $message;

   /** @var Array Groupe liste des groupes  */
   private $groupes;
      
   /** @var Array Lieux liste des lieux  */
   private $lieux;


    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=representation&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="85%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>

                <?php
                // En cas de création, l'id est accessible à la saisie           
                if ($this->actionRecue == "creer") {
                    // On a le souci de ré-afficher l'id tel qu'il a été saisi
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" value="<?= $this->uneRepresentation->getId() ?>" name="id" size ="8" maxlength="4"></td>
                    </tr>
                    <?php
                } else {
                    // sinon l'id est dans un champ caché 
                    ?>
                    <tr>
                        <td><input type="hidden" value="<?= $this->uneRepresentation->getId(); ?>" name="id"></td><td></td>
                    </tr>
                    <?php
                }
                ?>
                <tr class="ligneTabNonQuad">
                    <td> Nom du groupe* : </td>

                    <td>
                        <SELECT name="groupes" id="groupes">
                            <?php 
                            
                            foreach ($this->groupes as $unGroupe){ ?>  
                            
                            <option value ="<?= $unGroupe->getId()?>"
                            
                            <?php if($this->uneRepresentation->getGroupe()->getId()==$unGroupe->getId()){ ?> selected <?php }?>><?=$unGroupe->getNom()?></option>
                            <?php
                            }
                            ?>
                        </SELECT>
                    </td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Nom du lieu* : </td>
                    <td>
                        <SELECT name="lieux" id="lieux">
                            <?php 
                            
                            foreach ($this->lieux as $unLieu){ ?>  
                            
                            <option value ="<?= $unLieu->getId()?>"
                            
                            <?php if($this->uneRepresentation->getLieu()->getId()==$unLieu->getId()){ ?> selected <?php }?>><?=$unLieu->getNom()?></option>
                            <?php
                            }
                            ?>
                        </SELECT>
                    </td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Date de representation (JJ:MM:AAAA)* : </td>
                    <td><input type="date" value="<?=$this->uneRepresentation->getDateRep() ?>" name="dateRep" 
                               min="2000-01-01" max="2150-12-31"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure de debut (HH:MM:SS)* : </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getHeureD() ?>" name="heureD" size="7" 
                               ></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure de fin (HH:MM:SS)* : </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getHeureF() ?>" name="heureF" size="7" 
                               ></td>
                </tr>

            </table>

            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <a href="index.php?controleur=representation&action=consulter">Retour</a>
        </form>
        <?php
        include $this->getPied();
    }

    public function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }


    public function setActionRecue(string $action) {
        $this->actionRecue = $action;
    }

    public function setActionAEnvoyer(string $action) {
        $this->actionAEnvoyer = $action;
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }
    
    function setGroupes(Array $groupes) {
        $this->groupes = $groupes;
    }

    function setLieux(Array $lieux) {
        $this->lieux = $lieux;
    }



}
